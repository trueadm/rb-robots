
function createArrayWithSize(size) {
	return Array.from(Array(size));
}

function convertEmptyToNull(array) {
	return array.length === 0 ? null : array;
}

function createGrid(width, height, scents, robot) {
	return createArrayWithSize(width).map((_, y) =>
		createArrayWithSize(height).map((_, x) => 
			robot.x === x && robot.y === y ? robot : (
				convertEmptyToNull(scents.filter(scent => 
					scent.x === x && scent.y === y
				))
			)
		)
	);
}

function updateGride(grid, scents, robot) {
	const width = grid.length;
	const height = grid[0].length;

	return createGrid(width, height, scents, robot);
}

const Directions = {
	N: 0,
	E: 1,
	S: 2,
	W: 3
};

const ActionTypes = {
	CREATE_GRID: 0,
	NEW_ROBOT: 1,
	INSTRUCTIONS: 2
};

const ActorTypes = {
	ROBOT: 0,
	SCENT: 1
};

function addScent(x, y) {
	return {
		type: ActorTypes.SCENT,
		x,
		y
	};
}

function createRobot(x, y, direction) {
	return {
		type: ActorTypes.ROBOT,
		x: +x,
		y: +y,
		direction
	};
}

const initialState = { robot: null, scents: [], grid: null, message: null };

function worldReducer(state = initialState, action) {
	switch (action.type) {
		case ActionTypes.CREATE_GRID:
			const { width, height } = action;

			return {
				robot: null,
				scents: [],
				grid: createGrid(width, height, [], []),
				message: null
			};
		case ActionTypes.NEW_ROBOT: {
			const { x, y, direction } = action;
			const robot = createRobot(x, y, direction);
			const scents = state.scents.slice();

			return {
				robot,
				scents,
				grid: updateGride(state.grid, scents, robot),
				message: null
			};
		} case ActionTypes.INSTRUCTIONS: {
			const { instructions } = action;
			const { robot, scents, message } = runInstructions(
				state.robot, 
				instructions, 
				state.scents,
				state.grid.length,
				state.grid[0].length
			);

			return {
				robot,
				scents,
				grid: updateGride(state.grid, scents, robot),
				message
			};
		} default:
			return state;
	}
}

function rotate(positive, direction) {
	if (direction === 'N') {
		if (positive) {
			return 'E';
		} else {
			return 'W';
		}
	} else if (direction === 'E') {
		if (positive) {
			return 'S';
		} else {
			return 'N';
		}
	} if (direction === 'S') {
		if (positive) {
			return 'W';
		} else {
			return 'E';
		}
	} if (direction === 'W') {
		if (positive) {
			return 'N';
		} else {
			return 'S';
		}
	}
}

function isOutOfBounds(x, y, width, height) {
	return x < 0 || y < 0 || x > width || y > height;
}

function runInstructions({ x, y, direction }, instructions, lastScents, width, height) {
	let lastX;
	let lastY;
	let isLost = false;

	for (let instruction of instructions) {
		lastX = x;
		lastY = y;

		switch (instruction) {
			case 'L':
				direction = rotate(false, direction);
				break;
			case 'R':
				direction = rotate(true, direction);
				break;
			case 'F':
				if (direction === 'N') {
					y++;
				} else if (direction === 'E') {
					x++;
				} else if (direction === 'S') {
					y--;
				} else if (direction === 'W') {
					x--;
				}
				break;
			default:
				throw Error('Bad instruction');
		}
		if (isOutOfBounds(x, y, width, height)) {
			if (hasScentAtPosition(lastX, lastY, lastScents)) {
				x = lastX;
				y = lastY;
			} else {
				isLost = true;
				break;
			}
		}
	}
	const nextScents = lastScents.slice();

	if (isLost) {
		nextScents.push({ x: lastX, y: lastY });
	}
	return {
		robot: { x, y, direction },
		scents: nextScents,
		message: (
			`${ isLost ? lastX : x } ${ isLost ? lastY : y } ${ direction } ${ isLost ? 'LOST' : '' }`
		)
	};
}

function hasScentAtPosition(x, y, scenets) {
	return scenets.filter(scent => scent.x === x && scent.y === y).length > 0;
}

function createDispatcher(app) {
	return function dispatcher(action) {
		app.state = worldReducer(app.state, action);

		if (app.state.message) {
			console.log(app.state.message);
		}
	}
}

function createDispatchCreation(dispatcher) {
	return function dispatchCreation([ width, height ]) {
		dispatcher({
			type: ActionTypes.CREATE_GRID,
			width: +width,
			height: +height
		});
	};
}

function createDispatchInstructions(dispatcher) {
	return function dispatchInstructions([ instructions ]) {
		dispatcher({
			type: ActionTypes.INSTRUCTIONS,
			instructions: instructions.split('')
		});
	};
}

function createDispatchNewRobot(dispatcher) {
	return function dispatchNewRobot([ x, y, direction ]) {
		dispatcher({
			type: ActionTypes.NEW_ROBOT,
			x,
			y,
			direction
		});
	};
}

function isInstructionCommand(command) {
	return command.length === 1;
}

function isCreationCommand(command) {
	return command.length === 2;
}

function isNewRobotCommand(command) {
	return command.length === 3;
}

function renderCell(cell) {
	if (!cell) {
		return '-';
	} else {
		// TODO check type for Robot or Scent
		return cell.direction;
	}
}

function render(state, debugMode) {
	const grid = state.grid;
	const message = state.message;

	if (grid && debugMode) {
		const world = ['\n'].concat(grid.reverse()).reduce((world, x) => 
			world + ['\n'].concat(x).reduce((world, cell) => world + renderCell(cell))
		);
		console.log('\nDEBUG: ', world, '\n');
	}
	if (message) {
		console.log(message);
	}	
}

function createWorld(dispatcher, onUpdate) {
	const dispatchCreation = createDispatchCreation(dispatcher);
	const dispatchInstructions = createDispatchInstructions(dispatcher);
	const dispatchNewRobot = createDispatchNewRobot(dispatcher);

	return function newCommand(command) {
		if (isCreationCommand(command)) {
			dispatchCreation(command);
		} else if (isInstructionCommand(command)) {
			dispatchInstructions(command);
		} else if (isNewRobotCommand(command)) {
			dispatchNewRobot(command);
		} else {
			process.exit(0);
		}
		onUpdate(render);
	};
}

function createInput(newCommand) {
	const stdin = process.openStdin();

	stdin.addListener("data", function(data) {
		newCommand(data.toString().trim().split(' '));
	});
}

function createApp(debugMode) {
	const app = { state: {} };
	const dispatcher = createDispatcher(app);
	const newCommand = createWorld(
		dispatcher, 
		renderer => renderer(app.state, debugMode),
		debugMode 
	);

	console.log('Developer Programming Problem - By Dominic Gannaway\n');
	createInput(newCommand);
	return app;
}

createApp(true);

